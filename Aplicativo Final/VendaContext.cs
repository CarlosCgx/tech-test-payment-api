using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projeto_Final_Pottencial.Entities;
using Microsoft.EntityFrameworkCore;

namespace Projeto_Final_Pottencial.Context
{
    public class VendaContext : DbContext
    {
        public VendaContext(DbContextOptions<VendaContext> options) : base(options)
        {

        }

        public DbSet<Venda> Vendas{ get; set; }
    }
}