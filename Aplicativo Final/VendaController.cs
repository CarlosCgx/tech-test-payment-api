using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks; 
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Projeto_Final_Pottencial.Entities;
using Projeto_Final_Pottencial.Context;

namespace Projeto_Final_Pottencial.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }


        // Registrar Venda.
        [HttpPost]
        public IActionResult Create(Venda venda)
        {
            _context.Add(venda);
            _context.SaveChanges();
            return Ok(nameof(ObterPorId), new {id = venda.Id}, venda);
        }

        // Buscar pelo Id da Venda
        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            
            if (venda == null)
                return NotFound();
            
            return Ok(venda);
        }

        // Atualizar Status da Venda
        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);
            
            if (vendaBanco == null)
                return NotFound();

            vendaBanco.StatusPedido = venda.StatusPedido;
            
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return  Ok(vendaBanco);
            
        }

    }
}