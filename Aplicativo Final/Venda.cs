using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Projeto_Final_Pottencial.Entities
{
    public class Venda
    {         
        public int Id { get; set; }
        public string Vendedor { get; set; }
        public string ItemVendido { get; set; }
        public string StatusPedido { get; set; }
        public DateTime DataVenda { get; set; }
    }

}